# MirrorNote

This app is written in swift on xcode


It's an OCR and TTS App, using Google Vision!

An app for university students who want to take notes and avoid the tedious act of writing; in a fast and efficient way.

The main problem that our interviewers hated was losing focus and time taking notes instead of listening to their professor.

With Mirrornotes that’s not a problem anymore.
You can just take your phone, take a picture, and you’re done.

![](Video/1.MP4)