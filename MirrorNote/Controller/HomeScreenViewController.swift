

import UIKit
import DrawerView
import AVFoundation
import BCGenieEffect
import ChameleonFramework
import AVKit

class ViewController: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate,AVSpeechSynthesizerDelegate,DrawerViewDelegate {
    var flag = true
    static let event = EventManager()
    @IBOutlet weak var btn: UIButton!
    var player: AVPlayer!
    var avpController = AVPlayerViewController()
    
    @IBOutlet weak var video_view: UIView!
    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var dialogBox: UITextView!
    @IBOutlet weak var character: UIImageView!
    let imagePicker = UIImagePickerController()
    
    @IBOutlet weak var button_scan: UIButton!
    
    @IBOutlet weak var button_stop: UIButton!
    
    @IBOutlet weak var button_pause: UIButton!
    
   
   
    func speechSynthesizer(_ synthesizer: AVSpeechSynthesizer, willSpeakRangeOfSpeechString characterRange: NSRange, utterance: AVSpeechUtterance) {
        let mutableAttributedString = NSMutableAttributedString(string: utterance.speechString)
        mutableAttributedString.addAttribute(.foregroundColor, value: UIColor.yellow, range: characterRange)
        dialogBox.scrollRangeToVisible(characterRange)
        dialogBox.attributedText = mutableAttributedString
    }
    
    @IBAction func stopbutton(_ sender: Any) {
        synthesizer.stopSpeaking(at: .immediate)
        UIView.animate(withDuration: 0.2) {
            self.button_stop.alpha = 0
            self.button_pause.alpha = 0
            self.dialogBox.text = self.dialogBox.text
            
        }
    }
    @IBAction func playPause(_ sender: Any) {
        if flag {
            synthesizer.pauseSpeaking(at: .immediate)
            flag = false
        }else{
            synthesizer.continueSpeaking()
          flag = true
        }
        
    }
    func speechSynthesizer(_ synthesizer: AVSpeechSynthesizer, didFinish utterance: AVSpeechUtterance) {
        dialogBox.attributedText = NSAttributedString(string: utterance.speechString)
        
    }
    
    func drawerDidMove(_ drawerView: DrawerView, drawerOffset: CGFloat) {
        if drawerOffset > contLine(){
            drawerView.scrollToPositionUser(convertPositionToDrawer(view: view,position: contLine()), animated: false)
        }
    }
    
    
    override func viewDidLoad() {
        
        button_scan.layer.borderColor = UIColor.blue.cgColor
        button_scan.layer.borderWidth = 1
        button_scan.layer.cornerRadius = 5.0
       
        //        greetPlayer(label: dialogBox, chImgView: character)
       
        synthesizer.delegate = self
        //Drawer View Setup
        //button_scan.layer.zPosition = 10
        let drawerViewController = self.storyboard!.instantiateViewController(withIdentifier: "DrawerViewController")
        let Drawer = self.addDrawerView(withViewController: drawerViewController)
        Drawer.overlayVisibilityBehavior = .disabled
        Drawer.delegate = self
        super.viewDidLoad()
        imagePicker.delegate = self
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .camera
        imageView.alpha = 0
      
        video_view.layer.frame = CGRect()
        self.button_pause.alpha = 0
        self.button_stop.alpha = 0
        btn.layer.zPosition = 9
        self.view.backgroundColor = UIColor(gradientStyle:UIGradientStyle.topToBottom, withFrame:view.frame, andColors:[UIColor(red:0.31, green:0.80, blue:0.77, alpha:1.0)
            ,UIColor(red:0.97, green:1.00, blue:0.97, alpha:1.0)])
        
        
     
        
       
        ViewController.event.listenTo(eventName: "changeText") {
            self.changetext()
        }
        
    
    }
    
    @IBAction func tapped(_ sender: Any) {
        present(imagePicker, animated: true, completion: nil)
        
    }
    
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            
            let img = GoogleCloudOCR().resize(image: pickedImage, to: view!.frame.size)
            self.imageView.genieOutTransition(withDuration: 0.01, start: self.btn.frame, start: .top, completion: nil)
            GoogleCloudOCR().detect(from: img!) { ocrResult in
                
                guard let ocrResult = ocrResult else {
                    print("Did not recognize any text in this image")
                    return
                }
                let Text = ocrResult.annotations[0].text
                self.dialogBox.text = Text
                self.dialogBox.alpha = 1.0
                
                self.dialogBox.genieOutTransition(withDuration: 0.7, start: self.btn.frame, start: BCRectEdge.top, completion: {
                    //                    self.avpController.player?.pause()
                    self.video_view.frame = CGRect()
                    let alert = alertWithTFBuilder(testo: self.dialogBox.text)
                    self.present(alert, animated:  true, completion:nil)
                })
                
                
                
            }
            imageView.image = pickedImage
            imageView.contentMode = .scaleAspectFill
            imageView.layer.cornerRadius = 10
            imageView.alpha = 1.0
            dialogBox.alpha = 0.0
        }
        
        dismiss(animated: true, completion: {
            self.imageView.genieInTransition(withDuration: 0.7, destinationRect: self.btn.frame, destinationEdge: BCRectEdge.top, completion:{
                // self.video_view.layer.frame = self.character.layer.frame
                UIView.animate(withDuration: 0.1) {
                    self.imageView.alpha = 0.0
                }
                
            })
            
            self.dialogBox.genieInTransition(withDuration: 0.01, destinationRect: self.btn.frame, destinationEdge: BCRectEdge.top, completion:nil)
        })
        
        
    }
    
    @objc func changetext (){
        let value = Helper.dialog
        dialogBox.text = value 
        stopSpeak()
        readFromScannedText(textToRead: value )
        UIView.animate(withDuration: 0.2) {
            self.button_pause.alpha = 1
            self.button_stop.alpha = 1
        }
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
        
    }
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
}



