

import Foundation
import UIKit

extension UILabel {
    
    func setTextWithTypeAnimation(typedText: String, characterDelay: TimeInterval = 5.0, imgView : UIImageView) {
        text = ""
        var writingTask: DispatchWorkItem?
        writingTask = DispatchWorkItem { [weak weakSelf = self] in
            for character in typedText {
                DispatchQueue.main.async {
                    if (imgView.image == UIImage(named:"standing"))
                    {
                        imgView.image = UIImage (named:"talking")
                    }

                    else {
                         imgView.image = UIImage (named:"standing")
                    }
                    weakSelf?.text!.append(character)
                }
                Thread.sleep(forTimeInterval: characterDelay/100)
            }
        }

        if let task = writingTask {
            let queue = DispatchQueue(label: "typespeed", qos: DispatchQoS.userInteractive)
            queue.asyncAfter(deadline: .now() + 0.05, execute: task)
        }
    }

}
